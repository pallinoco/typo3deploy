<?php
/**
 * This file is part of a Pallino Project.
 *
 * It is a reserved code the Pallino & Co. has the intellectual property
 * of the code.
 *
 * Each not authorized usage is prohibited.
 */

/**
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 04/04/16
 */

namespace Pallino\TYPO3Deploy\Test;




class SshTest extends \PHPUnit_Framework_TestCase {

    public function testCreation() {
//
        $mock = $this->getMockBuilder('Pallino\TYPO3Deploy\Utility\Ssh')
            ->disableOriginalConstructor()
            ->getMock();

      // set expectations for constructor calls
      $mock->expects($this->once())
          ->method('login');

      // now call the constructor
      $reflectedClass = new \ReflectionClass('Pallino\TYPO3Deploy\Utility\Ssh');
      $constructor = $reflectedClass->getConstructor();
      $constructor->invoke($mock,'spica.immaginario.com','pippo',22,5,'','path_to_file');
    }
}
