<?php

/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

namespace Pallino\TYPO3Deploy\Command;

use Pallino\TYPO3Deploy\Configuration\DefaultConfiguration;
use Pallino\TYPO3Deploy\Utility\Debug;
use Pallino\TYPO3Deploy\Utility\Git;
use Pallino\TYPO3Deploy\Utility\PathFinder;
use Pallino\TYPO3Deploy\Utility\Release;
use Pallino\TYPO3Deploy\Utility\Report;
use Pallino\TYPO3Deploy\Utility\Ssh;
use Pallino\TYPO3Deploy\Utility\Upload;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;
use Pallino\TYPO3Deploy\Utility\Typo3;


/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */
class DeployCommand extends \Symfony\Component\Console\Command\Command {

    const SSH_CONNECTION_ERROR = 1000;
    const GIT_DEPLOY_ERROR = 1001;

    const PRE_GIT = 1;
    const GIT = 2;
    const CREATE_FOLDER = 3;
    const UPLOAD_FILE = 4;
    const CREATE_LINKS = 5;

    protected $status = 0;

    /**
     * @var Ssh
     */
    protected $sshServer;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * @var \Symfony\Component\Console\Output\OutputInterface
     */
    protected $terminal;

    /**
     * @var string
     */
    protected $configurationFile;

    /**
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    protected $progressBar;

    /**
     * @var Git
     */
    protected $git;

    /**
     * @var Release
     */
    protected $release;

    /**
     * @var bool
     */
    protected $production = false;

    /**
     * @var stage
     */
    protected $stage = false;

    /**
     * @var string
     */
    protected $configFolder;

    protected function configure() {
        $this
            ->setName('deploy')
            ->setDescription('deploy')
            ->addArgument('config', InputArgument::OPTIONAL, 'path to folder with configuration file', getcwd())
            ->addOption(
                'op',
                null,
                InputOption::VALUE_REQUIRED,
                'Command to execute [deploy|lastrelease|releases|updateVersion|TYPO3Packages|TYPO3ActivePackages]',
                'deploy'
            )
            ->addOption(
                'release',
                null,
                InputOption::VALUE_REQUIRED,
                'Release to update version',
                ''
            )
            ->addOption(
                'v',
                null,
                InputOption::VALUE_REQUIRED,
                'Version of Deploy',
                ''
            )
            ->addOption(
                'env',
                null,
                InputOption::VALUE_OPTIONAL,
                'Environment',
                ''
            );
    }

    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output) {

        $fs = new Filesystem();
        try {
            $this->status = self::PRE_GIT;
            $this->configFolder = $input->getArgument('config') . DIRECTORY_SEPARATOR;
            $this->configurationFile = $this->configFolder . 'config.yml';
            $this->terminal = $output;
            Report::init($this->terminal);
            switch ($input->getOption('env')) {
                case 'stage':
                    $this->stage = true;
                    Report::setEvent('Use Staging environment');
                    break;
                case 'production':
                    $this->production = true;
                    Report::setEvent('Use Production environment');
                    break;
                default:
                    Report::setEvent('Use Default environment');
                    break;
            }
            $this->bootstrap();
            $typo3 = new Typo3($this->sshServer, $this->configuration);
            //$typo3->getPackageContent();
            switch ($input->getOption('op')) {
                case 'deploy':
                    $this->deploy($input->getOption('v'));
                    break;
                case 'lastrelease':
                    $this->getLastRelease();
                    break;
                case 'releases':
                    $this->printAllRelease();
                    break;
                case 'updateVersion':
                    if (($release = $input->getOption('release')) != '') {
                        $version = $input->getOption('v');
                        $foundRelease = $this->release->getSpecificRelease($release);
                        if ($foundRelease == null) Report::setError('release not found.');
                        $foundRelease['version'] = $version;
                        $this->release->updateRelease($foundRelease);
                    } else {
                        Report::setError('version not defined.');
                    }
                    break;
                case 'TYPO3Packages':
                    $typo3->showListOfPackages();
                    break;
                case 'TYPO3ActivePackages':
                    $typo3->showListOfPackages(true);
                    break;
                case 'TYPO3Configuration':
                    $typo3->showConfigurationItems();
                    break;

            }
            $fs->remove(array(LOCAL_TEMP));

        } catch (\Exception $e) {
            $header_style = new OutputFormatterStyle('red', 'black', array('bold'));
            $output->getFormatter()->setStyle('header', $header_style);
            switch ($e->getCode()) {
                case self::SSH_CONNECTION_ERROR:
                    $message = 'Impossible establish a connection with remote host: ' . $this->configuration['ssh']['host'];
                    break;
                default:
                    $message = $e->getMessage();
            }
            $output->writeln('<error>' . $message . '</error>');
            $this->rollback();
        }
    }

    protected function getLastRelease() {

        Report::setEvent('fetch last release:');
        $lastRelease = $this->release->getLastReleaseStructure();
        $this->printRelease($lastRelease, true);
    }

    protected function printAllRelease() {
        Report::setEvent('fetch all release:');
        $releases = $this->release->getDeploiesList();
        ksort($releases);
        end($releases);
        $last = current($releases);
        reset($releases);
        foreach ($releases as $release) {
            $current = ($release['folder'] == $last['folder']);
            $this->printRelease($release, $current);
        }
    }

    protected function printRelease($release, $current = false) {
        $current = ($current) ? '* Current Release' : 'Release';
        Report::setInfo(sprintf("%s is: %s", $current, $release['folder']), false);
        Report::setInfo(sprintf("\tDate: %s", $release['date']->format('d-m-Y H:i:s')), false);
        Report::setInfo(sprintf("\tRepository: %s", $release['repository']), false);
        Report::setInfo(sprintf("\tBranch: %s", $release['branch']), false);
        Report::setInfo(sprintf("\tCommit: %s", $release['sha1']), false);
        Report::setInfo(sprintf("\tVersion: %s", $release['version']), false);
    }

    protected function setSshServer() {
        if ($this->configuration['ssh']['useKey']) {
            $this->sshServer = new Ssh($this->configuration['ssh']['host'], $this->configuration['ssh']['user'], $this->configuration['ssh']['port'], $this->configuration['general']['socketTimeout'], '', $this->configuration['ssh']['privateKey']);
        } else {
            $this->sshServer = new Ssh($this->configuration['ssh']['host'], $this->configuration['ssh']['user'], $this->configuration['ssh']['port'], $this->configuration['general']['socketTimeout'], $this->configuration['ssh']['password']);
        }
        if (!$this->sshServer->isAuthenticated()) {
            throw new \Exception('SSH connection error', self::SSH_CONNECTION_ERROR);
        }
        $this->sshServer->getSsh()->setTimeout($this->configuration['general']['socketCommandTimeout']);
    }

    protected function setGit() {
        $this->git = new \Pallino\TYPO3Deploy\Utility\Git($this->configuration['git']['repository'], $this->configuration['ssh']['remotePath'], $this->configuration['git']['branch']);
        $this->git->setSshRemoteServer($this->sshServer);
        $this->git->setTerminalObject($this->terminal);
    }

    protected function setRelease() {
        $this->release = new Release($this->sshServer);
    }

    protected function setEnvironment() {
        ini_set("default_socket_timeout", $this->configuration['general']['socketTimeout']);
        Debug::setTerminalObject($this->terminal);
        Debug::enableDebug($this->configuration['general']['debug']);
        define('REMOTE_FOLDER', $this->configuration['ssh']['remotePath']);
        define('RELEASES_FOLDER', 'releases');
        define('RELEASE_REMOTE_PATH', REMOTE_FOLDER . DIRECTORY_SEPARATOR . RELEASES_FOLDER);
        define('SHARED_FOLDER', 'shared');
        define('SHARED_REMOTE_PATH', REMOTE_FOLDER . DIRECTORY_SEPARATOR . SHARED_FOLDER);
        define('ROOT', getcwd());
        define('LOCAL_TEMP', ROOT . DIRECTORY_SEPARATOR . 'temporary');
    }

    protected function bootstrap() {
        if (!file_exists($this->configurationFile)) {
            $this->terminal->writeln(sprintf('<error>The configuration file: %s doesn\'t exist.</error>', $this->configurationFile));
            exit;
        }
        $config = Yaml::parse(
            file_get_contents($this->configurationFile)
        );
        if ($this->production) {
            $productionConfig = $this->configFolder . 'config_prod.yml';
            if (file_exists($productionConfig)) {
                $configProduction = Yaml::parse(
                    file_get_contents($productionConfig)
                );
                $config = array_replace_recursive($config, $configProduction);
            } elseif ($this->stage) {
                $stageConfig = $this->configFolder . 'config_stage.yml';
                if (file_exists($stageConfig)) {
                    $configStage = Yaml::parse(
                        file_get_contents($stageConfig)
                    );
                    $config = array_replace_recursive($config, $configStage);
                } else {
                    Report::setInfo('production configuration not found use default');
                }
            } else {
                Report::setInfo('production configuration not found use default');
            }
        }
        $configs = array($config);
        $processor = new Processor();
        $configuration = new DefaultConfiguration();
        $this->configuration = $processor->processConfiguration(
            $configuration,
            $configs
        );
        $this->setEnvironment();
        $this->setSshServer();
        $this->setGit();
        $this->setRelease();
        define('CURRENTRELEASE',RELEASE_REMOTE_PATH . DIRECTORY_SEPARATOR . $this->release->getLastRelease());
        if (file_exists(LOCAL_TEMP)) {
            throw new \Exception('temporary folder already exists');
        } else {
            mkdir(LOCAL_TEMP);
        }
    }

    public function progress($sent) {
        //echo $sent . "2222\n";

        $this->progressBar->advance($sent - $this->progressBar->getProgress());
    }

    /**
     * @param string $version
     */
    protected function deploy($version) {
        Report::setEvent('executing deploy');
        $this->release->getDeploiesList();
        $this->git->setVersion($version);
        $this->git->getHead();
        $this->status = self::GIT;
        $this->git->get();
        $this->release->addRelease($this->git->getRelease($version));
        $this->release->updateReleasesFile();
        $this->status = self::CREATE_FOLDER;
        Report::setEvent('Uploading files');
        $upload = new Upload($this->sshServer, $this->configuration);
        $upload->createFolder(SHARED_REMOTE_PATH);
        foreach ($this->configuration['folders'] as $key => $value) {
            foreach ($value as $subKey => $subValue) {
                $this->configuration['folders'][$key][$subKey] = PathFinder::substitutePath($subValue);
            }
            $upload->createFolderWithLink($this->configuration['folders'][$key]);
        }
        $this->status = self::GIT;
        foreach ($this->configuration['uploads'] as $key => $value) {
            foreach ($value as $subKey => $subValue) {
                $this->configuration['uploads'][$key][$subKey] = PathFinder::substitutePath($subValue);
            }
            $upload->copy($this->configuration['uploads'][$key]);
        }
        Report::setEvent('Uploading files completed');
        Report::setEvent('Creating links');
        $this->status = self::CREATE_LINKS;
       // sleep(10);
        foreach ($this->configuration['links'] as $key => $value) {
            foreach ($value as $subKey => $subValue) {
                $this->configuration['links'][$key][$subKey] = PathFinder::substitutePath($subValue);
            }
            $upload->link($this->configuration['links'][$key]);
        }
        Report::setEvent('Creating links completed');

        $this->release->maintainLastReleased($this->configuration['general']['releaseToRemove']);
        Report::setEvent('Move current deploy');
        $this->release->moveCurrent();
        Report::setEvent('Deploy successfully completed');
    }

    public function rollback() {
        $fs = new Filesystem();
        Report::setEvent('Rolling Back', true);
        switch ($this->status) {
            case self::PRE_GIT:
                $rolling_text = 'Deploy rolled back';
                break;
            case self::GIT:
                $gitFolder = $this->git->getReleaseFullRemotePath();
                $this->sshServer->getSftp()->rmdir($gitFolder);
                $lastRelease = $this->release->getLastRelease();
                $rolling_text = sprintf('Deploy rolled back to %s', $lastRelease);
                break;
        }
        $fs->remove(array(LOCAL_TEMP));
        Report::setEvent($rolling_text);
    }


}