<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */
namespace Pallino\TYPO3Deploy\Configuration;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class DefaultConfiguration implements ConfigurationInterface
{
    /**
     * Set tree of configuration file
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('deploy');
        $rootNode
            ->children()
                ->arrayNode('ssh')
                     ->children()
                        ->scalarNode('host')->isRequired()->end()
                        ->scalarNode('user')->isRequired()->end()
                        ->booleanNode('useKey')->defaultValue(false)->end()
                        ->scalarNode('password')->defaultValue('')->end()
                        ->scalarNode('port')->defaultValue(22)->end()
                        ->scalarNode('publicKey')->defaultValue('')->end()
                        ->scalarNode('privateKey')->defaultValue('')->end()
                        ->scalarNode('remotePath')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('git')
                    ->children()
                        ->scalarNode('repository')->isRequired()->end()
                        ->scalarNode('branch')->defaultValue('master')->end()
                    ->end()
                ->end()
                ->arrayNode('general')
                    ->children()
                        ->booleanNode('debug')->defaultValue(false)->end()
                        ->booleanNode('boxSecurity')->defaultValue(true)->end()
                        ->scalarNode('releaseToRemove')->defaultValue(3)->end()
                        ->scalarNode('socketTimeout')->defaultValue(5)->end()
                        ->scalarNode('socketCommandTimeout')->defaultValue(300)->end()
                    ->end()
                ->end()
                ->arrayNode('TYPO3')
                    ->children()
                        ->scalarNode('environmentClass')->defaultValue('')->end()
                        ->scalarNode('packagesPath')->defaultValue('')->end()
                        ->scalarNode('localConfigurationPath')->defaultValue('')->end()
                    ->end()
                ->end()
                ->arrayNode('uploads')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('localPath')->end()
                            ->scalarNode('remotePath')->end()
                            ->scalarNode('addLink')->defaultValue('')->end()
                            ->booleanNode('overwrite')->defaultValue(true)->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('folders')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('remotePath')->end()
                            ->scalarNode('addLink')->defaultValue('')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('links')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('remotePath')->end()
                            ->scalarNode('addLink')->defaultValue('')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
        return $treeBuilder;
    }
}

