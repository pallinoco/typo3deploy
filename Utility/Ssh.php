<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */

namespace Pallino\TYPO3Deploy\Utility;


class Ssh {

    /**
     * @var string hostname
     */
    protected $host;

    /**
     * @var string ssh user
     */
    protected $user;

    /**
     * @var int port number
     */
    protected $port;

    /**
     * @var int timeout for connection
     */
    protected $timeout;

    /**
     * @var string ssh password
     */
    protected $password;

    /**
     * @var string key file
     */
    protected $keyFile;

    /**
     * @var bool true use keyfile
     */
    protected $useKey;

    /**
     * @var bool true indicate access granted to ssh server
     */
    protected $accessGranted = false;

    /**
     * @var int last exit status of command executed by ssh
     */
    protected $lastExitStatus;

    /**
     * @var string last error string returned by command executed by ssh
     */
    protected $lastErrorMessage;

    /**
     * @var \phpseclib\Net\SSH2
     */
    protected $sshObject;

    /**
     * @var \phpseclib\Net\SFTP
     */
    protected $sftpObject;


    /**
     * Ssh constructor.
     */
    public function __construct($host, $user, $port, $timeout = 10, $password = '', $keyFile = '') {
        $this->host = $host;
        $this->user = $user;
        $this->port = $port;
        $this->password = $password;
        $this->timeout = $timeout;
        $this->keyFile = $keyFile;
        $this->sshObject = new \phpseclib\Net\SSH2($this->host, $this->port, $this->timeout);
        $this->sftpObject = new \phpseclib\Net\SFTP($this->host,$this->port,$this->timeout);
        $this->login();

    }

    /**
     * Login method to access ssh server
     * @return bool
     */
    public function login() {
        if ($this->accessGranted) return true;
        if ($this->password) {
            if ($this->sshObject->login($this->user, $this->password) && $this->sftpObject->login($this->user, $this->password)) {
                $this->accessGranted = true;
                return true;
            }
        } elseif ($this->keyFile) {
            $sshKey = new \phpseclib\Crypt\RSA();
            $sshKey->loadKey(file_get_contents($this->keyFile));
            if ($this->sshObject->login($this->user, $sshKey) && $this->sftpObject->login($this->user, $sshKey)) {
                $this->accessGranted = true;
                return true;
            }
        }
        return false;
    }

    /**
     * Upload a string to remote file
     *
     * @param string $content string to upload
     * @param string $remoteFile path to remote file
     * @return bool true if successfully uploaded
     */
    public function uploadString($content,$remoteFile) {
        return $this->sftpObject->put($remoteFile,$content);
    }

    /**
     * Upload a local file to remote file
     *
     * @param string $localFile path to local file
     * @param string $remoteFile path to remote file
     * @return bool true if successfully uploaded
     */
    public function upload($localFile,$remoteFile) {
        if(!file_exists($localFile)){
            $this->lastErrorMessage = 'file ' . $localFile . ' doesn\'t exist in the local filesystem';
            return false;
        }
        return $this->sftpObject->put($remoteFile,file_get_contents($localFile));
    }

    /**
     * Download a file via ssh
     *
     * @param string $remoteFile path to remote file
     * @param string $localFile path to local file
     * @return bool|mixed true if successfully downloaded
     */
    public function download($remoteFile,$localFile) {
        if(!$this->sftpObject->file_exists($remoteFile)){
            $this->lastErrorMessage = 'file ' . $remoteFile . ' doesn\'t exist in the remote server';
            return false;
        }
        return $this->sftpObject->get($remoteFile,$localFile);
    }

    /**
     * return the authentication status
     *
     * @return bool true if already authenticated
     */
    public function isAuthenticated() {
        return $this->accessGranted;
    }

    /**
     * Execute a command via ssh
     *
     * @param string $cmd
     * @return bool|string id successfully execute return the output from remote
     */
    public function exec($cmd) {
        if ($this->accessGranted) {
            $result = $this->sshObject->exec($cmd);
            $this->lastErrorMessage = $this->sshObject->getStdError();
            $this->lastExitStatus = $this->sshObject->getExitStatus();
            return $result;
        }
        return false;
    }

    /**
     * Return the last error message from last ssh command
     *
     * @return string
     */
    public function getLastError() {
        return $this->lastErrorMessage;
    }

    /**
     * Return the last exit status code from last ssh command
     *
     * @return string
     */
    public function getLastStatus() {
        return $this->lastExitStatus;
    }

    /**
     * Execute a command via ssh and return the status into an array
     * --> [output] contains the output from command (generally return the error message if an error raise)
     * --> [error] contains the output from stderror
     * --> [statusCode] contains the integer status code from command
     *
     * @param string $cmd command to execute
     * @return array|bool the array if access granted, otherwise false
     */
    public function execWithReturnStructure($cmd) {
        if ($this->accessGranted) {
            return array('output' => $this->exec($cmd), 'error' => $this->lastErrorMessage, 'statusCode' => $this->lastExitStatus);
        }
        return false;
    }

    /**
     * return the sFtp object to directly call
     *
     * @return \phpseclib\Net\SFTP
     */
    public function getSftp() {
        return $this->sftpObject;
    }

    /**
     * Returns the Ssh object to directly call
     *
     * @return \phpseclib\Net\SSH2
     */
    public function getSsh() {
        return $this->sshObject;
    }
}