<?php
/**
 * This file is part of a Pallino Project.
 *
 * It is a reserved code the Pallino & Co. has the intellectual property
 * of the code.
 *
 * Each not authorized usage is prohibited.
 */

/**
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 08/03/16
 */

namespace Pallino\TYPO3Deploy\Utility;


class PathFinder {

    public static function substitutePath($path) {
        $path = str_replace('<ROOT>',ROOT,$path);
        $path = str_replace('<SHARED>',SHARED_REMOTE_PATH,$path);
        $path = str_replace('<RELEASE>',RELEASE_REMOTE_PATH . DIRECTORY_SEPARATOR . CURRENT_RELEASE,$path);
        $path = str_replace('<REMOTEFOLDER>',REMOTE_FOLDER,$path);
        $path = str_replace('<CURRENTRELEASE>',CURRENTRELEASE,$path);
        return $path;
    }
}