<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */

namespace Pallino\TYPO3Deploy\Utility;


class Upload {

    /**
     * @var \Pallino\TYPO3Deploy\Utility\Ssh
     */
    protected $ssh;

    /**
     * @var array configuration array
     */
    protected $configuration;

    /**
     * @var string home folder for box security
     */
    protected $homeFolder;

    /**
     * @var bool true if sanitize folder
     */
    protected $boxSecurity = false;

    /**
     * Upload constructor.
     */
    public function __construct(\Pallino\TYPO3Deploy\Utility\Ssh $ssh, array $configuration) {
        $this->ssh = $ssh;
        $this->configuration = $configuration;
        $this->boxSecurity = $this->configuration['general']['boxSecurity'];
        $this->homeFolder = $this->configuration['ssh']['remotePath'];
    }

    /**
     * File to copy in specificstructure
     * --> [local_file] is the path to local file
     * --> [remote_file] is the path to remote file
     * --> [addLink] is the optional path for link
     * --> [overwrite] is the optional boolean variable identify the overwriting behaviour (true can overwrite)
     *
     * @param array $file
     */
    public function copy(array $file) {
        Report::setText(sprintf("\t" . '[*] Copying file %s ... ',$file['localPath']),true);
        $folder = dirname($file['remotePath']);
        if($this->boxSecurity && !$this->sanitizeFolder($folder)){
            Report::setComment('denied by box security folder');
            Debug::writeln(sprintf('%s not contains %s home folder',$file['remotePath'],$this->homeFolder));
            return;
        }
        if(!$this->ssh->getSftp()->file_exists($folder)){
            $this->ssh->getSftp()->mkdir($folder,-1,true);
        }
        if(!isset($file['overwrite'])){
            $overwrite = true;
        }
        else{
            $overwrite = $file['overwrite'];
        }
        if(!$overwrite && $this->ssh->getSftp()->file_exists($file['remotePath'])){
            Report::setComment('denied by overwrite flag');
            return;
        }
        if($this->ssh->upload($file['localPath'],$file['remotePath']) !== false){
            Report::setInfo('successfully');
        }
        else{
            Report::setError('failed');
        }
        if(strlen($file['addLink'])){
            Report::setText(sprintf("\t" . '[*] Creating link %s ... ',$file['addLink']),true);
            if($this->ssh->getSftp()->symlink($file['remotePath'],$file['addLink'])){
                Report::setInfo('successfully');
            }
            else{
                Report::setError('failed');
            }
        }
    }

    /**
     * File to copy in specificstructure
     * --> [local_file] is the path to local file
     * --> [remote_file] is the path to remote file
     * --> [addLink] is the optional path for link
     * --> [overwrite] is the optional boolean variable identify the overwriting behaviour (true can overwrite)
     *
     * @param array $file
     * @throws \Exception
     */
    public function link(array $file) {
        Report::setText(sprintf("\t" . '[*] making link file %s to %s ... ',$file['remotePath'],$file['addLink']),true);
        if(!$this->ssh->getSftp()->file_exists($file['remotePath'])){
            Report::setError('failed');
            Debug::writeln(sprintf('%s remote path doesn\' exists',$file['remotePath']));
            return;
        }
        if($this->ssh->getSftp()->file_exists($file['addLink'])){
            Report::setError('failed');
            Debug::writeln(sprintf('%s destination link already exists',$file['addLink']));
            return;
        }
        $folder = dirname($file['remotePath']);
        if($this->boxSecurity && !$this->sanitizeFolder($folder)){
            Report::setComment('denied by box security folder');
            Debug::writeln(sprintf('%s not contains %s home folder',$file['remotePath'],$this->homeFolder));
            return;
        }
        if(strlen($file['addLink'])){
            //removed used of symlink because an error
            //$report = $this->ssh->exec(sprintf('ln -s %s %s',$file['remotePath'],$file['addLink']));
            if($this->ssh->getSftp()->symlink($file['remotePath'],$file['addLink'])){
            //if($report['status'] == 0){
                Report::setInfo('successfully');
            }
            else{
                Report::setError('failed');
            }
        }
    }

    public function createFolderWithLink($remoteFolder) {
        Report::setText(sprintf("\t" . '[*] creating folder %s ... ',$remoteFolder['remotePath']),true);
        if($this->boxSecurity && !$this->sanitizeFolder($remoteFolder['remotePath'])){
            Report::setComment('denied by box security folder');
            Debug::writeln(sprintf('%s not contains %s home folder',$remoteFolder['remotePath'],$this->homeFolder));
            return;
        }
        if(!$this->ssh->getSftp()->file_exists($remoteFolder['remotePath'])){
            $this->ssh->getSftp()->mkdir($remoteFolder['remotePath'],-1,true);
            Report::setInfo('successfully');
        }
        else{
            Report::setComment('folder already exists');
        }
        if(strlen($remoteFolder['addLink'])){
            Report::setText(sprintf("\t" . '[*] Creating link %s ... ',$remoteFolder['addLink']),true);
            if($this->ssh->getSftp()->symlink($remoteFolder['remotePath'],$remoteFolder['addLink'])){
                Report::setInfo('successfully');
            }
            else{
                Report::setError('failed');
            }
        }
        else{
            Report::setInfo('successfully');
        }
    }

    /**
     * Create a folder
     * @param string $folder
     */
    public function createFolder($folder) {
        if(!$this->ssh->getSftp()->file_exists($folder)){
            Report::setText(sprintf("\t" . 'Creating folder %s ... ',$folder),true);
            if($this->ssh->getSftp()->mkdir($folder,0777,true)){
                Report::setInfo('successfully');
            }
            else{
                Report::setError('failed');
            }
        }
    }

    /**
     * @param string $folder folder to check
     * @return bool true if folder is secured otherwise false
     */
    public function sanitizeFolder($folder) {
        if(strpos($folder,$this->homeFolder) === 0){
            return true;
        }
        else{
            return false;
        }
    }

}