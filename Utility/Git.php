<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 
 */ 

namespace Pallino\TYPO3Deploy\Utility;

use Symfony\Component\Console\Output\OutputInterface;

class Git {

    /**
     * @var string the remote folder for clone
     */
    protected $remoteFolderName;

    /**
     * @var string name of the branch
     */
    protected $branch;

    /**
     * @var string repository full path
     */
    protected $fullRepositoryPath;

    /**
     * @var string name of the last commit associated with deploy
     */
    protected $commit;

    /**
     * @var string name of the folder where deploy will be saved
     */
    protected $releaseName;

    /**
     * @var \Datetime date of the deploy
     */
    protected $releaseDate;

    /**
     * @var \Pallino\TYPO3Deploy\Utility\Ssh
     */
    protected $ssh;

    /**
     * @var boolean true if debug is enabled
     */
    protected $debug;

    /**
     * @var  OutputInterface
     */
    protected $terminal;


    /**
     * @var string full path for release folders
     */
    protected $releasesRemoteFolder;

    /**
     * @var string full path for the release folder
     */
    protected $releaseRemoteFolder;

    /**
     * @var string version passed from command line
     */
    protected $version;

    /**
     * Git constructor.
     * @param string $repositoryName repository url
     * @param string $remoteFolderName remote folder to deploy
     * @param string $branch name of the branch
     */
    function __construct($repositoryName, $remoteFolderName, $branch = 'master') {
        if ($repositoryName && $remoteFolderName && $branch) {
            $this->setup($repositoryName, $remoteFolderName, $branch);
        }
        $this->releaseDate = new \DateTime();
        $this->releaseName = $this->releaseDate->format('YmdHisu');
        define('CURRENT_RELEASE',$this->releaseName);
        $this->releasesRemoteFolder = $this->remoteFolderName . DIRECTORY_SEPARATOR . RELEASES_FOLDER;
        $this->releaseRemoteFolder = $this->releasesRemoteFolder . DIRECTORY_SEPARATOR . $this->releaseName;
    }

    /**
     * Set the Ssh object
     * @param Ssh $server Ssh object
     */
    public function setsshRemoteServer(\Pallino\TYPO3Deploy\Utility\Ssh $server) {
        $this->ssh = $server;
    }

    /**
     * Set the debug mode
     * @param boolean $debug
     */
    public function setDebugMode($debug) {
        $this->debug = $debug;
    }

    /**
     * Set the terminal object
     * @param OutputInterface $terminal terminal object
     */
    public function setTerminalObject(OutputInterface $terminal) {
        $this->terminal = $terminal;
    }


    /**
     * Optional function to set main git properties
     *
     * @param string $repositoryName repository url
     * @param string $remoteFolderName remote folder to deploy
     * @param string $branch name of the branch
     */
    public function setup($repositoryName, $remoteFolderName, $branch = 'master') {
        $this->remoteFolderName = $remoteFolderName;
        $this->branch = $branch;
        $this->fullRepositoryPath = $repositoryName;
        $this->version = '';
    }

    /**
     * Set version of deploy
     *
     * @param string $version version to associate to this deploy
     */
    public function setVersion($version) {
        $this->version = $version;
    }

    /**
     * Set last commit of the chosen branch
     *
     * @return string
     * @throws \Exception
     */
    public function getHead() {
        try {
            $command = sprintf('git ls-remote %s %s 2>&1', $this->fullRepositoryPath, $this->branch);
            Debug::writeln($command);
            $output = $this->ssh->exec($command);
            $matches = preg_split("/[\t,]+/", $output);
            $this->commit = $matches[0];
            Debug::writeln(sprintf('Ahead fetched: %s', $this->commit));
            return $this->commit;
        } catch (\Exception $e) {
            throw new \Exception(sprintf('<error>Error during receiving HEAD of branch %s</error>', $this->branch));
        }

    }

    /**
     * Executes the git clone operation
     *
     * @throws \Exception
     */
    public function get() {
        try {
            if(!$this->ssh->getSftp()->file_exists($this->releaseRemoteFolder)){
                $this->ssh->getSftp()->mkdir($this->releaseRemoteFolder);
            }
            $linefeed = true;
            if($this->debug) $linefeed = false;

            Report::setComment(sprintf('cloning repository %s with branch: %s ... ', $this->fullRepositoryPath, $this->branch),$linefeed);
            $cmd = 'git clone -q -b ' . $this->branch . ' ' . $this->fullRepositoryPath . ' ' . $this->releaseRemoteFolder . ' && cd ' . $this->releaseRemoteFolder . ' && git checkout -q -b deploy ' . $this->commit . ' && git submodule -q init && git submodule -q sync && git submodule -q update --init --recursive && (echo ' . $this->commit . ' > ' . $this->releaseRemoteFolder . DIRECTORY_SEPARATOR . 'REVISION)';
            Debug::writeln($cmd);
            $output = $this->ssh->execWithReturnStructure($cmd);
            if($output['statusCode']>0){
                throw new \Exception();
            }
            Report::setInfo(sprintf('cloned successfully'));
        } catch (\Exception $e) {
            $error = 'unknown error';
            if(is_array($output)){
                $error = $output['error'];
            }
            throw new \Exception('git clone failure: ' . $error);
        }
    }

    /**
     * Gets the Release
     * @return array the release structure of the actual release
     */
    public function getRelease() {
        return array('folder' => $this->releaseName, 'date' => $this->releaseDate, 'sha1' => $this->commit, 'repository' => $this->fullRepositoryPath, 'branch' => $this->branch, 'version' => $this->version);
    }

    /**
     * Gets the release timestamp name
     * @return string the name of the release folder
     */
    public function getReleaseName() {
        return $this->releaseName;
    }

    /**
     * Return the full path to remote folder
     * @return string
     */
    public function getReleaseFullRemotePath() {
        return $this->releaseRemoteFolder;
    }

    /**
     * Writes debug message with break line
     * @param string $text text to write on debug
     */
    protected function debug($text) {
        if($this->debug){
            $this->terminal->write($text);
        }
    }

    /**
     * Writes debug message without break line
     * @param string $text text to write on debug
     */
    protected function debugln($text) {
        if($this->debug){
            $this->terminal->writeln($text);
        }
    }
}