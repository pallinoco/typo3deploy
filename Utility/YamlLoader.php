<?php

namespace Pallino\TYPO3Deploy\Utility;
/**
 * This file is part of a Pallino Project.
 *
 * It is a reserved code the Pallino & Co. has the intellectual property
 * of the code.
 *
 * Each not authorized usage is prohibited.
 */

/**
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 03/03/16
 */

    use Symfony\Component\Config\Loader\FileLoader;
    use Symfony\Component\Yaml\Yaml;

    class YamlLoader extends FileLoader
{
    public function load($resource, $type = null)
    {
        $configValues = Yaml::parse(file_get_contents($resource));

        // ... handle the config values

        // maybe import some other resource:

        // $this->import('extra_users.yml');
    }

    public function supports($resource, $type = null)
    {
        return is_string($resource) && 'yml' === pathinfo(
            $resource,
            PATHINFO_EXTENSION
        );
    }
}