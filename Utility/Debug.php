<?php
/**
 * This file is part of a Pallino Project.
 *
 * It is a reserved code the Pallino & Co. has the intellectual property
 * of the code.
 *
 * Each not authorized usage is prohibited.
 */

/**
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 04/03/16
 */

namespace Pallino\TYPO3Deploy\Utility;


use Symfony\Component\Console\Output\OutputInterface;

class Debug {

    /**
     * @var boolean
     */
    protected static $enable = false;

    /**
     * @var OutputInterface
     */
    protected static $terminal;


    public static function setTerminalObject(OutputInterface $terminal) {
        self::$terminal = $terminal;
    }

    public static function enableDebug($enable) {
        self::$enable = $enable;
    }

    public static function write($text) {
        if(self::$enable){
            Report::setDebug($text,true);
        }
    }

    public static function writeln($text) {
        if(self::$enable){
            Report::setDebug($text,false);
        }
    }
}