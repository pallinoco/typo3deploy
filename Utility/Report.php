<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */

namespace Pallino\TYPO3Deploy\Utility;


use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class Report {

    /**
     * @var OutputInterface
     */
    protected static $terminal;

    /**
     * @var bool if true next label is inline (without additional spaces
     */
    protected static $commentInLine = false;


    /**
     * Sets terminal object
     * @param OutputInterface $terminal
     */
    public static function setTerminalObject(OutputInterface $terminal) {
        self::$terminal = $terminal;
    }

    /**
     * Initialize terminal object
     * @param OutputInterface $terminal
     */
    public static function init(OutputInterface $terminal) {
        self::$terminal = $terminal;
        $event_style = new OutputFormatterStyle('green', 'black', array('bold'));
        self::$terminal->getFormatter()->setStyle('event', $event_style);
        $error_style = new OutputFormatterStyle('red', 'black', array('bold'));
        self::$terminal->getFormatter()->setStyle('error', $error_style);
        $error_style = new OutputFormatterStyle('cyan', 'black');
        self::$terminal->getFormatter()->setStyle('debug', $error_style);
        $error_style = new OutputFormatterStyle('green', 'black', array('bold'));
        self::$terminal->getFormatter()->setStyle('infob', $error_style);
    }

    /**
     * Write text to terminal using event block
     * @param string $text
     */
    public static function setEvent($text,$error = false) {
        $date = new \DateTime();
        $tag = ($error)?'error':'event';
        $text = sprintf('<%s>--> %s %s</%s>',$tag,$date->format('Y-m-d H:i:s'),$text,$tag);
        self::$terminal->writeln($text);
    }

    /**
     * Write text to terminal using comment block
     * @param string $text
     */
    public static function setComment($text,$inline = false) {
        $spaces = (self::$commentInLine)?'':'    ';
        $text = sprintf('<comment>%s%s</comment>',$spaces,$text);
        if($inline){
            self::$terminal->write($text);
            self::$commentInLine = true;
        }
        else{
            self::$terminal->writeln($text);
            self::$commentInLine = false;
        }
    }



    /**
     * Write text to terminal using event block but without -->
     * @param string $text
     */
    public static function setInformation($text,$inline = false) {
        $spaces = (self::$commentInLine)?'':'    ';
        $text = sprintf('<event>%s%s</event>',$spaces,$text);
        if($inline){
            self::$terminal->write($text);
            self::$commentInLine = true;
        }
        else{
            self::$terminal->writeln($text);
            self::$commentInLine = false;
        }
    }


    /**
     * Write text to terminal without any block
     * @param string $text
     */
    public static function setText($text,$inline = false) {
        $spaces = (self::$commentInLine)?'':'    ';
        $text = sprintf('%s%s',$spaces,$text);
        if($inline){
            self::$terminal->write($text);
            self::$commentInLine = true;
        }
        else {
            self::$terminal->writeln($text);
            self::$commentInLine = false;
        }
    }


    /**
     * Write text to terminal using info block
     * @param string $text
     */
    public static function setInfo($text,$inline = false,$bold = false) {
        $spaces = (self::$commentInLine)?'':'    ';
        $tag = ($bold)?'infob':'info';
        $text = sprintf('<' . $tag . '>%s%s</' . $tag . '>',$spaces,$text);
        if($inline){
            self::$terminal->write($text);
            self::$commentInLine = true;
        }
        else {
            self::$terminal->writeln($text);
            self::$commentInLine = false;
        }
    }


    /**
     * Write text to terminal using error block
     * @param string $text
     */
    public static function setError($text,$inline = false) {
        $spaces = (self::$commentInLine)?'':'    ';
        $text = sprintf('<error>%s%s</error>',$spaces,$text);
        if($inline){
            self::$terminal->write($text);
            self::$commentInLine = true;
        }
        else {
            self::$terminal->writeln($text);
            self::$commentInLine = false;
        }
    }


    /**
     * Write text to terminal using error block
     * @param string $text
     */
    public static function setDebug($text,$inline = false,$header = false) {
        $spaces = '    ';
        if($inline){
            $text = ($header)?sprintf('<debug>%s-->Debug: %s</debug>', $spaces,$text):sprintf('<debug>%s</debug>', $text);
            self::$terminal->write($text);
            self::$commentInLine = true;
        }
        else {
            if (self::$commentInLine) {
                self::$terminal->writeln('');
            }
            $text = sprintf('<debug>%s-->Debug: %s</debug>', $spaces, $text);
            self::$terminal->writeln($text);
            self::$commentInLine = false;
        }
    }
}