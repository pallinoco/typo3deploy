<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */

namespace Pallino\TYPO3Deploy\Utility;


use Pallino\TYPO3Deploy\Utility\Ssh;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Pallino\TYPO3Deploy\Utility\PathFinder;

/**
 * Class Typo3
 * @package typo3deploy\Utility
 */
class Typo3 {

    /**
     * @var \Pallino\TYPO3Deploy\Utility\Ssh
     */
    protected $sshServer;

    /**
     * @var array
     */
    protected $deployConfiguration;

    /**
     * @var array list of TYPO3 packages
     */
    protected $packages = array();

    /**
     * @var array list of TYPO3 packages
     */
    protected $configuration = array();


    /**
     * Typo3 constructor.
     * @param Ssh $sshServer server SSH
     * @param array $configuration general deploy configuration
     */
    public function __construct(Ssh $sshServer, array $configuration) {
        $this->sshServer = $sshServer;
        $this->deployConfiguration = $configuration;
    }

    /**
     * Set the list of Packages of TYPO3 installation
     * @throws \Exception
     */
    public function getPackageContent() {

        if(!isset($this->deployConfiguration['TYPO3']['packagesPath'])){
            throw new \Exception('TYPO3 packages path missed.');
        }
        $file = PathFinder::substitutePath($this->deployConfiguration['TYPO3']['packagesPath']);
        if(!$this->sshServer->getSftp()->file_exists(PathFinder::substitutePath($this->deployConfiguration['TYPO3']['packagesPath']))){
            throw new \Exception('TYPO3 packages file doesn\'t exists.');
        }
        $this->sshServer->getSftp()->get($file,LOCAL_TEMP . DIRECTORY_SEPARATOR . 'package.php');
        $this->packages = include(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'package.php');
        unlink(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'package.php');
        
    }

    /**
     * Set the Local TYPO3 Configuration
     * @throws \Exception
     */
    public function getLocalConfigurationContent() {

        if(!isset($this->deployConfiguration['TYPO3']['localConfigurationPath'])){
            throw new \Exception('TYPO3 packages path missed.');
        }
        $file = PathFinder::substitutePath($this->deployConfiguration['TYPO3']['localConfigurationPath']);
        if(!$this->sshServer->getSftp()->file_exists(PathFinder::substitutePath($this->deployConfiguration['TYPO3']['localConfigurationPath']))){
            throw new \Exception('TYPO3 packages file doesn\'t exists.');
        }
        $this->sshServer->getSftp()->get($file,LOCAL_TEMP . DIRECTORY_SEPARATOR . 'localConfiguration.php');
        $this->configuration = include(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'localConfiguration.php');
        unlink(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'localConfiguration.php');

    }

    /**
     * Set the Local TYPO3 Configuration
     * @throws \Exception
     */
    public function getVersion() {

        if(!isset($this->deployConfiguration['TYPO3']['environmentClass'])){
            throw new \Exception('TYPO3 environment class path missed.');
        }
        $file = PathFinder::substitutePath($this->deployConfiguration['TYPO3']['environmentClass']);
        if(!$this->sshServer->getSftp()->file_exists(PathFinder::substitutePath($this->deployConfiguration['TYPO3']['environmentClass']))){
            throw new \Exception('TYPO3 environment class file doesn\'t exists.');
        }
        $this->sshServer->getSftp()->get($file,LOCAL_TEMP . DIRECTORY_SEPARATOR . 'SystemEnvironmentBuilder.php');
        include(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'SystemEnvironmentBuilder.php');
        $class = <<<EOT
<?php
namespace Pallino\TYPO3Deploy;
  class SystemEnvironmentBuilder extends \TYPO3\CMS\Core\Core\SystemEnvironmentBuilder{
    static public function setEnv(){
        self::defineBaseConstants();
    }
  }
EOT;
        file_put_contents(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'SystemEnvironmentBuilder1.php',$class);
        include(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'SystemEnvironmentBuilder1.php');
        \Pallino\TYPO3Deploy\SystemEnvironmentBuilder::setEnv();
        unlink(LOCAL_TEMP . DIRECTORY_SEPARATOR . 'SystemEnvironmentBuilder.php');

    }

    /**
     * Show list of extensions (if you set true the onlyactive show only active extensions)
     * @param bool $onlyactive see above
     * @throws \Exception
     */
    public function showListOfPackages($onlyactive = false) {
        if(count($this->packages) == 0){
            $this->getPackageContent();
            ksort($this->packages['packages']);
        }
        Report::setEvent('List of TYPO3 Packages');
        foreach($this->packages['packages'] as $packageName => $package){
            if($onlyactive && $package['state'] === 'inactive') continue;
            Report::setInfo(sprintf('Package: %s',$packageName),false,true);
            if($package['state'] == 'active'){
                Report::setInfo(sprintf('    State: %s',$package['state']));
            }
            else{
                Report::setError(sprintf('    State: %s',$package['state']));
            }
            Report::setInfo(sprintf('    Path: %s',$package['packagePath']));
        }
    }

    /**
     * Shows list of configuration items
     * @throws \Exception
     */
    public function showConfigurationItems() {
        if(count($this->configuration) == 0){
            $this->getLocalConfigurationContent();
        }
        if(!defined('TYPO3_version')){
            $this->getVersion();
        }
        Report::setEvent('List of TYPO3 Configuration');
        Report::setEvent('TYPO3 Version');
        Report::setInfo(sprintf('    Version: %s',TYPO3_version));
        Report::setInfo(sprintf('    Branch: %s',TYPO3_branch));
        Report::setInfo(sprintf('    Copyright: %s',TYPO3_copyright_year));
        Report::setEvent('Database Configuration');
        foreach($this->configuration['DB'] as $key => $value){
            if(is_array($value)) continue;
            Report::setInfo(sprintf('    %s: %s',$key,$value));
        }
        Report::setEvent('System Configuration');
        foreach($this->configuration['SYS'] as $key => $value){
            if(is_array($value)) continue;
            Report::setInfo(sprintf('    %s: %s',$key,$value));
        }
        Report::setEvent('Graphic Configuration');
        foreach($this->configuration['GFX'] as $key => $value){
            if(is_array($value)) continue;
            Report::setInfo(sprintf('    %s: %s',$key,$value));
        }
        Report::setEvent('Frontend Configuration');
        foreach($this->configuration['FE'] as $key => $value){
            if(is_array($value)) continue;
            Report::setInfo(sprintf('    %s: %s',$key,$value));
        }
        Report::setEvent('Backend Configuration');
        foreach($this->configuration['BE'] as $key => $value){
            if(is_array($value)) continue;
            Report::setInfo(sprintf('    %s: %s',$key,$value));
        }
    }
}