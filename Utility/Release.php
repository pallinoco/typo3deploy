<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package DEPLOY
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */

namespace Pallino\TYPO3Deploy\Utility;


class Release {

    /**
     * @var array
     */
    protected $releases = array();

    /**
     * @var \Pallino\TYPO3Deploy\Utility\Ssh
     */
    protected $ssh;

    /**
     * @var string
     */
    protected $releasesRemoteFolder;

    /**
     * @var array
     */
    protected $lastRelease;

    /**
     * Release constructor.
     * @param \Pallino\TYPO3Deploy\Utility\Ssh $session
     */
    public function __construct(\Pallino\TYPO3Deploy\Utility\Ssh $session) {
        $this->sFtp = $session->getSftp();
        $this->ssh = $session;
        $this->releasesRemoteFolder = REMOTE_FOLDER . DIRECTORY_SEPARATOR . RELEASES_FOLDER;
    }


    /**
     * @return array list of version released
     * @throws \Exception
     */
    public function getDeploiesList() {
        if (count($this->releases)) return $this->releases;
        if ($this->sFtp->file_exists(REMOTE_FOLDER . DIRECTORY_SEPARATOR . 'release')) {
            $releases = array();
            $output = $this->ssh->execWithReturnStructure('cat ' . REMOTE_FOLDER . DIRECTORY_SEPARATOR . 'release');
            $output = explode("\n",$output['output']);
            if (is_array($output)) {
                foreach ($output as $row) {
                    $columns = explode(',', $row);
                    if (count($columns) == 6) {
                        $date = new \DateTime($columns[1]);
                        $releases[$date->getTimestamp()] = array('folder' => $columns[0], 'date' => $date, 'sha1' => $columns[2], 'repository' => $columns[3], 'branch' => $columns[4], 'version' => $columns[5]);
                    } else {
                        throw new \Exception('Remote releases file in wrong format.');
                    }
                }
            } else {
                throw new \Exception('Remote action to fetch releases failed.');
            }
            ksort($releases);
            $this->releases = $releases;

        }
        return $this->releases;
    }

    /**
     * Add a release
     * @param array $release
     */
    public function addRelease($release) {
        $this->releases[$release['date']->getTimestamp()] = $release;
    }

    /**
     * Remove all releases except last $count
     * @param int $count release to maintain
     * @throws \Exception
     */
    public function maintainLastReleased($count = 3) {
        if (count($this->releases) == 0) {
            $this->getDeploiesList();
        }
        ksort($this->releases);
        if (count($this->releases)) {
            $current = $this->sFtp->readlink(REMOTE_FOLDER . DIRECTORY_SEPARATOR . 'current');
            $currentArray = explode(DIRECTORY_SEPARATOR,$current);
            $current = array_pop($currentArray);
            $releasesRemoved = 0;
            foreach ($this->releases as $key => $release) {
                if (count($this->releases) <= $count) break;
                if($release['folder'] != $current) {
                    if ($this->sFtp->file_exists($this->releasesRemoteFolder . DIRECTORY_SEPARATOR . $release['folder'])) {
                        try{
                           $this->ssh->execWithReturnStructure(sprintf('rm -rf %s',$this->releasesRemoteFolder . DIRECTORY_SEPARATOR . $release['folder']));
                            $releasesRemoved++;
                            unset($this->releases[$key]);
                            Debug::writeln(sprintf('removed release %s of %s', $release['folder'], $release['date']->format('d/m/Y H:i:s')));
                        }
                        catch(\Exception $e){
                            Debug::writeln(sprintf('Error during removing remote release folder %s.', $this->releasesRemoteFolder . DIRECTORY_SEPARATOR . $release['folder']));
                        }
                    } else {
                        Debug::writeln(sprintf('Remote release folder %s doesn\'t exist', $release['folder']));
                    }
                }
            }
            $this->updateReleasesFile();
            Report::setEvent(sprintf('%d releases removed',$releasesRemoved));
        }
    }

    /**
     * Update remote releases file based on $this->releases
     */
    public function updateReleasesFile() {
        foreach ($this->releases as $key => $release) {
            $row[] = $release['folder'] . ',' . $release['date']->format('d-m-Y H:i:s') . ',' . $release['sha1'] . ',' . $release['repository'] . ',' . $release['branch'] . ',' . $release['version'];
        }

        if($this->ssh->uploadString(implode("\n", $row),REMOTE_FOLDER . DIRECTORY_SEPARATOR . 'release')){
            Debug::writeln('Updated releases file');
        }
        else{
            Report::setError('Error during save releases to remote release file');
        }
    }

    /**
     * Get last release
     * @return mixed|null
     * @throws \Exception
     */
    public function getLastReleaseStructure() {
        $this->getDeploiesList();
        if(count($this->releases)){
            end($this->releases);
            $release = current($this->releases);
            return $release;
        }
        return null;
    }

    /**
     * Get last release timestamp name
     * @return null
     * @throws \Exception
     */
    public function getLastRelease() {
        $this->getDeploiesList();
        if(count($this->releases)){
            end($this->releases);
            $release = current($this->releases);
            return $release['folder'];
        }
        return null;
    }

    /**
     * Move current to last release
     * @return bool
     */
    public function moveCurrent() {
        $destination = $this->releasesRemoteFolder . DIRECTORY_SEPARATOR . $this->getLastRelease();
        if($this->ssh->getSftp()->file_exists($destination)){
            if($this->ssh->getSftp()->is_link(REMOTE_FOLDER . DIRECTORY_SEPARATOR . 'current')) {
                $this->ssh->getSftp()->delete(REMOTE_FOLDER . DIRECTORY_SEPARATOR . 'current');
            }
            $this->ssh->getSftp()->symlink($destination,REMOTE_FOLDER . DIRECTORY_SEPARATOR . 'current');
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Return list of releases
     * @return array
     */
    public function getReleases() {
        return $this->releases;
    }

    public function getSpecificRelease($release) {
        $this->getDeploiesList();
        if(count($this->releases)){
            $date = \DateTime::createFromFormat('YmdHisu',$release);
            if(array_key_exists($date->getTimestamp(),$this->releases)){
                return $this->releases[$date->getTimestamp()];
            }
        }
        return null;
    }

    public function updateRelease($release) {
        if(array_key_exists($release['date']->getTimestamp(),$this->releases)){
            $this->releases[$release['date']->getTimestamp()] = $release;
            $this->updateReleasesFile();
            return true;
        }
        else{
            return false;
        }
    }
}