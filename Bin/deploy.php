<?php
require __DIR__.'/vendor/autoload.php';

use Pallino\TYPO3Deploy\Command\DeployCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new DeployCommand());
$application->run();